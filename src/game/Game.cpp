#include "Game.h"
#include <raylib.h>

void GlobalGame::clean()
{
    delete Game.assetManager;
    delete Game.window;
    delete Game.keyPressHandler;
    delete Game.tileManager;
}


GlobalGame::GlobalGame()
{
    Game.camera = { 0 };
    Game.camera.target = (Vector2) { 0, 0 };
    Game.camera.zoom = 3.0f;
}

GlobalGame Game;
