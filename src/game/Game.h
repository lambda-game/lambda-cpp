#pragma once
#include "../gfx/assets/AssetManager.h"
#include "../gfx/Window.h"
#include "../gfx/input/KeyPressHandler.h"
#include "../world/Worlds.h"
#include "../tiles/TileManager.h"
#include "../entitys/Entity.h"
#include "../entitys/PlayerEntity.h"
#include "../entitys/EntityManager.h"
#include <raylib.h>
#include <vector>


class GlobalGame
{
public:

    GlobalGame();

    // game
    AssetManager* assetManager;
    TileManager* tileManager;
    KeyPressHandler* keyPressHandler = new KeyPressHandler();
    Window* window;

    // player
    PlayerEntity* player;
    Camera2D camera;
    Rectangle scissorArea = { 0, 0, 300, 300 };

    // worlds
    Overworld* overworld = new Overworld();
    int renderedTiles;

    // entitys
    EntityManager* entityManager = new EntityManager();
    std::vector<Entity*> spawnedEntitys;

    // tiles
    int tileSize = 32; // every tile is 32x32 pixels in size

    // functions
    void clean(); // de-allocates all used resources in Game
};

extern GlobalGame Game;
