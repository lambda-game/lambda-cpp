#pragma once

class KeyPressHandler
{

public:
    KeyPressHandler();
    ~KeyPressHandler();

    void handleEvents();
};
