#include "KeyPressHandler.h"
#include <spdlog/spdlog.h>
#include "../../game/Game.h"
#include <raylib.h>

KeyPressHandler::KeyPressHandler()
{

}

KeyPressHandler::~KeyPressHandler()
{

}

void KeyPressHandler::handleEvents()
{
    if (IsKeyDown(KEY_W))
    {
        Game.player->setPosition(Game.player->getX(), Game.player->getY() - 4);
        Game.player->setDirection(Direction::NORTH);
    }

    if (IsKeyDown(KEY_A))
    {
        Game.player->setPosition(Game.player->getX() - 4, Game.player->getY());
        Game.player->setDirection(Direction::WEST);
    }

    if (IsKeyDown(KEY_D))
    {
        Game.player->setPosition(Game.player->getX() + 4, Game.player->getY());
        Game.player->setDirection(Direction::EAST);
    }

    if (IsKeyDown(KEY_S))
    {
        Game.player->setPosition(Game.player->getX(), Game.player->getY() + 4);
        Game.player->setDirection(Direction::SOUTH);
    }
}
