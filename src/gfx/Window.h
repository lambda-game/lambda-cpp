#pragma once

class Window {
public:
    Window();
    ~Window();
    void init(const char* title, bool fullscreen);
    void handleEvents();
    void render();
    void clean();
    void update();
    void quit();
    bool isRunning();
    int getWidth();
    int getHeight();
private:
    bool running;
};
