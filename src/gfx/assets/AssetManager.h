#pragma once

#include <map>
#include <iostream>
#include <raylib.h>

class AssetManager
{
public:
    AssetManager();
    ~AssetManager();
    void loadTexture(std::string filePath);
    Texture2D getTexture(std::string filePath);
private:
    std::map<std::string, Texture2D> textures;
};
