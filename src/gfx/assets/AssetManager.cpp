#include "AssetManager.h"
#include <spdlog/spdlog.h>
#include <map>
#include <raylib.h>


AssetManager::~AssetManager()
{

}

AssetManager::AssetManager()
{

}

Texture2D AssetManager::getTexture(std::string filePath)
{
    return this->textures.at(filePath);
}

void AssetManager::loadTexture(std::string filePath)
{
    this->textures.insert(std::pair<std::string, Texture2D>(filePath, LoadTexture(filePath.c_str())));
    spdlog::info("Loaded Texture2d: {}", filePath);
}

