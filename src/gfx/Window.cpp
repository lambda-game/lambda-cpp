#include <spdlog/spdlog.h>
#include <raylib.h>
#include <string>
#include "Window.h"
#include "../game/Game.h"
#include "../entitys/EntityManager.h"

Window::Window()
{

}

Window::~Window()
{

}


int Window::getWidth()
{
  if (IsWindowFullscreen())
    {
        int monitor = GetCurrentMonitor();
        return GetMonitorWidth(monitor);
    }
    else
    {
        return GetScreenWidth();
    }
}

int Window::getHeight()
{
    if (IsWindowFullscreen())
    {
        int monitor = GetCurrentMonitor();
        return GetMonitorHeight(monitor);
    }
    else
    {
        return GetScreenHeight();
    }
}

void Window::init(const char* title, bool fullscreen)
{
    spdlog::info("Initializing Raylib");
    SetTraceLogLevel(LOG_NONE);
    SetConfigFlags(FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT);
    InitWindow(1000, 1000, title);

    if (fullscreen)
    {
        int monitor = GetCurrentMonitor();
        SetWindowSize(GetMonitorWidth(monitor), GetMonitorHeight(monitor));
        ToggleFullscreen();
    }

    SetTargetFPS(60);

    spdlog::info("Created window");
    running = true;
}

void Window::update()
{
    Game.camera.target = { (float) Game.player->getX(), (float) Game.player->getY() };
    Game.camera.offset = (Vector2){ getWidth()/2.0f, getHeight()/2.0f };
}



void Window::render()
{

    BeginDrawing();

    BeginMode2D(Game.camera);

    ClearBackground(BLACK);
    Game.overworld->render();
    Game.entityManager->renderEntitys();

    EndMode2D();

    DrawText(std::to_string(GetFPS()).c_str(), 0, 0, 20, LIGHTGRAY);
    DrawText(std::to_string(Game.renderedTiles).c_str(), 0, 20, 20, LIGHTGRAY);


    EndDrawing();
}

void Window::clean()
{
}

void Window::quit()
{
    spdlog::info("Shutting down window");
    running = false;
    spdlog::info("Cleaning up resources");
    clean();
}

void Window::handleEvents()
{
    Game.keyPressHandler->handleEvents();
}

bool Window::isRunning()
{
    return running;
}

