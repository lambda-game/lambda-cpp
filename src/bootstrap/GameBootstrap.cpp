#include "GameBootstrap.h"
#include "../gfx/assets/AssetManager.h"
#include "../game/Game.h"
#include "../tiles/SandTile.h"
#include "../world/generation/WorldGenerator.h"
#include "../entitys/PlayerEntity.h"
#include <ctime>
#include <spdlog/spdlog.h>

void GameBootstrap::runBootstrap()
{
    spdlog::info("Loading assets");

    Game.assetManager = new AssetManager();

    Game.assetManager->loadTexture("assets/tiles/grass.png");
    Game.assetManager->loadTexture("assets/tiles/sand.png");
    Game.assetManager->loadTexture("assets/tiles/water.png");
    Game.assetManager->loadTexture("assets/entitys/player/player_walk_up.png");
    Game.assetManager->loadTexture("assets/entitys/player/player_walk_down.png");
    Game.assetManager->loadTexture("assets/entitys/player/player_walk_left.png");
    Game.assetManager->loadTexture("assets/entitys/player/player_walk_right.png");


    Game.player = new PlayerEntity();
    Game.player->spawn();


    std::time_t time = std::time(0);

    spdlog::info("Generating world with seed: {}", time);

    WorldGenerator* gen = new WorldGenerator(time, 1000, 1000);
    std::vector<WorldTile*> worldData = gen->generateWorld();

    Game.overworld->setWorldData(worldData);

    delete gen;

    spdlog::info("Bootstrapped game!");
}
