#pragma once
#include "../../tiles/Tile.h"

struct WorldTile
{
    int x;
    int y;
    TileType type;

};
