#include "WorldGenerator.h"
#include "../../game/Game.h"
#include <spdlog/spdlog.h>
#include <vector>

WorldGenerator::WorldGenerator(int seed, int height, int width)
{
    this->seed = seed;
    this->height = height;
    this->width = width;
}

WorldGenerator::~WorldGenerator()
{

}

std::vector<WorldTile*> WorldGenerator::generateWorld()
{

  	const siv::PerlinNoise::seed_type worldSeed = this->seed;
    const siv::PerlinNoise perlin{ worldSeed };

    std::vector<WorldTile*> output;

    int water = 0;
    int grass = 0;
    int sand = 0;
    for (int y = 0; y < this->height; ++y)
	{
		for (int x = 0; x < this->width; ++x)
		{
			const double noise = perlin.octave2D_01((x * 0.01), (y * 0.01), 4);


            if (noise < 0.2)
            {

                WorldTile* tile = new WorldTile();
                tile->x = x*Game.tileSize;
                tile->y = y*Game.tileSize;
                tile->type = TileType::WATER;
                output.push_back(tile);
                water++;
            } else if (noise < 0.26)
            {
                WorldTile* tile = new WorldTile();
                tile->x = x*Game.tileSize;
                tile->y = y*Game.tileSize;
                tile->type = TileType::SAND;
                output.push_back(tile);
                sand++;
            }
            else
            {
                WorldTile* tile = new WorldTile();
                tile->x = x*Game.tileSize;
                tile->y = y*Game.tileSize;
                tile->type = TileType::GRASS;
                output.push_back(tile);
                grass++;
            }

		}

	}

	spdlog::info("Placed: {} grass, and {} water tiles", grass, water);

    return output;
}
