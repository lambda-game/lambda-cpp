#include "World.h"
#include "../game/Game.h"
#include <spdlog/spdlog.h>
#include <raylib.h>

void World::addTile(WorldTile* tile)
{
    worldData.insert(worldData.begin(), tile);
}

std::vector<WorldTile*> World::getWorldData()
{
    return worldData;
}

void World::render()
{
    Game.renderedTiles = 0;
    for (WorldTile* tile : worldData)
    {
        if (this->shouldRenderPoint(tile->x, tile->y))
        {
            Tile* realTile = Game.tileManager->getTileFromType(tile->type);
            DrawTexture(Game.assetManager->getTexture(realTile->getTexture()), tile->x, tile->y, WHITE);
            delete realTile;
            Game.renderedTiles++;
        }
    }
}

void World::setWorldData(std::vector<WorldTile*> data)
{
    worldData = data;
}

bool World::shouldRenderPoint(int x, int y)
{
    Rectangle rect;
    rect.height = Game.window->getHeight();
    rect.width = Game.window->getWidth();
    rect.x = Game.camera.target.x-Game.camera.offset.x;
    rect.y = Game.camera.target.y-Game.camera.offset.y;

    return CheckCollisionPointRec((Vector2){(float)x, (float)y}, rect);
}

void clean()
{

}


