#pragma once
#include "storage/WorldTile.h"
#include <vector>

class World
{
public:
    int getId();
    void onPlayerEnter();
    void addTile(WorldTile* tile);
    std::vector<WorldTile*> getWorldData();
    void render();
    void setWorldData(std::vector<WorldTile*> data);
    bool shouldRenderPoint(int x, int y);

private:
    std::vector<WorldTile*> worldData;
};
