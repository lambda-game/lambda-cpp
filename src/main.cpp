#include <iostream>
#include "gfx/Window.h"
#include "game/Game.h"
#include "bootstrap/GameBootstrap.h"

int main(int argc, char **argv) {
    Game.window = new Window();

    Game.window->init("LambdaSDL", true);

    GameBootstrap* bootstrapper = new GameBootstrap();
    bootstrapper->runBootstrap();

    delete bootstrapper; // no need for the bootstrapper anymore

    while (Game.window->isRunning()){
        Game.window->handleEvents();
        Game.window->update();
        Game.window->render();
    }

    Game.window->clean();
    Game.clean();
}
