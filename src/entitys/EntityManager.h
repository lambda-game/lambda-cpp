#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H

/**
 * @todo write docs
 */
class EntityManager
{
public:
    /**
     * Default constructor
     */
    EntityManager();

    /**
     * Destructor
     */
    ~EntityManager();

    void renderEntitys();
};

#endif // ENTITYMANAGER_H
