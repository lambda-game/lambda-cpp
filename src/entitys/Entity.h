#include <iostream>
#include "Direction.h"

#pragma once

class Entity
{
public:
    Entity();
    virtual ~Entity();

    virtual std::string getTexture(); // should be overriten by subclass
    Direction getDirection();
    void setDirection(Direction direction);
    int getX();
    int getY();
    void setPosition(int x, int y);
    void spawn();

private:
    Direction facingDirection = Direction::NORTH;
    int x;
    int y;
};
