#include "Entity.h"
#include <spdlog/spdlog.h>
#include "../game/Game.h"

// basic entity

Entity::Entity()
{

}

Entity::~Entity()
{

}

void Entity::setDirection(Direction d)
{
    this->facingDirection = d;
}

Direction Entity::getDirection()
{
    return this->facingDirection;
}

void Entity::setPosition(int x, int y)
{
    this->x = x;
    this->y = y;
}

int Entity::getX()
{
    return x;
}

int Entity::getY()
{
    return y;
}

void Entity::spawn()
{
    spdlog::info("Spawning entity");
    Game.spawnedEntitys.push_back(this);
}

std::string Entity::getTexture()
{
    spdlog::error("Failed to get texture: not overwritten by subclass!");
    exit(1);
}

