#include "EntityManager.h"
#include "../game/Game.h"
#include "PlayerEntity.h"
#include <spdlog/spdlog.h>

EntityManager::EntityManager()
{

}

EntityManager::~EntityManager()
{

}


void EntityManager::renderEntitys()
{
    for (Entity* entity : Game.spawnedEntitys)
    {
        DrawTexture(Game.assetManager->getTexture(entity->getTexture()), entity->getX(), entity->getY(), WHITE);
    }
}
