#pragma once
#include "Entity.h"


class PlayerEntity : public Entity
{
public:
    std::string getTexture() override
    {
        switch (getDirection())
        {
            case NORTH:
                return "assets/entitys/player/player_walk_up.png";
                break;
            case SOUTH:
                return "assets/entitys/player/player_walk_down.png";
                break;
            case EAST:
                return "assets/entitys/player/player_walk_right.png";
                break;
            case WEST:
                return "assets/entitys/player/player_walk_left.png";
                break;
            default:
                return NULL;
        }
    }
};
