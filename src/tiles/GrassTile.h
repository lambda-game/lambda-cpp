#pragma once
#include "Tile.h"

class GrassTile : public Tile
{
public:

    int getId() {
        return TileType::GRASS;
    }

    std::string getTexture() {
        return "assets/tiles/grass.png";
    }
};
