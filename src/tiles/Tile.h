#pragma once
#include <iostream>

enum TileType
{
    GRASS = 0,
    SAND = 1,
    WATER = 2
};

class Tile {
public:
    virtual int getId() { return 0; };
    virtual std::string getTexture() { return "assets/tiles/grass.png"; };
};
