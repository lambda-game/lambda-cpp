#pragma once
#include "Tile.h"

class WaterTile : public Tile
{
public:

    int getId() {
        return TileType::WATER;
    }

    std::string getTexture() {
        return "assets/tiles/water.png";
    }
};
