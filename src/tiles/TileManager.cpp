#include "TileManager.h"
#include "Tile.h"
#include "SandTile.h"
#include "WaterTile.h"
#include "GrassTile.h"
#include <spdlog/spdlog.h>


Tile* TileManager::getTileFromType(TileType type)
{
    switch (type){
        case SAND:
            return new SandTile();
            break;
        case WATER:
            return new WaterTile();
            break;
        case GRASS:
            return new GrassTile();
            break;
        default:
            return NULL;
    }
}
