#pragma once
#include "Tile.h"

class TileManager {
public:
    Tile* getTileFromType(TileType type);
};
