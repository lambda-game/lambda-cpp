#pragma once
#include "Tile.h"

class SandTile : public Tile
{
public:

    int getId() {
        return TileType::SAND;
    }

    std::string getTexture() {
        return "assets/tiles/sand.png";
    }
};
